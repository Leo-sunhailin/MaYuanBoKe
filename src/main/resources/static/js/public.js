/**
 * Created by Administrator on 2017/3/2.
 */
/**
 * Created by ml on 2017/3/2.
 */
$(function(){
    var backButton=$(".stick");
    backButton.on('click',function(){
        $('html,body').animate({
            scrollTop:0
        },500)
    });
    $(window).on('scroll',function(){
        if($(window).scrollTop()>$(window).height()/1.5){
            backButton.show();
        }else{
            backButton.hide();
        }
    });
});

var vm_header=new Vue({
    el:'#header-box',
    data:{
        researchText:'',
        researchType:'',
        researchUrl:'',
    },
    methods:{
        research_btn:function () {
            var researchText=this.researchText;
            var researchUrl=this.researchUrl;
            window.location.href=researchUrl+'?search='+researchText
        }
    }
});

