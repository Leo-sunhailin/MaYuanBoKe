package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ML on 2017/03/12.
 * 文库表
 */
@Document(collection = "my_library")
public class Library implements Serializable {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id;  //id

    private String libraryName;  //类型名

    private String libraryIntro;  //类型简介

    private String libraryCode;  //编号

    private String libraryImg; //类型图片

    private String libraryType; //类型分类

    private String libraryListUrl; //类型目录链接

    private String dynamicLibraryUrl; //动态图库链接

    private String addLibraryDate; //类型添加时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibraryIntro() {
        return libraryIntro;
    }

    public void setLibraryIntro(String libraryIntro) {
        this.libraryIntro = libraryIntro;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryImg() {
        return libraryImg;
    }

    public void setLibraryImg(String libraryImg) {
        this.libraryImg = libraryImg;
    }

    public String getLibraryType() {
        return libraryType;
    }

    public void setLibraryType(String libraryType) {
        this.libraryType = libraryType;
    }

    public String getLibraryListUrl() {
        return libraryListUrl;
    }

    public void setLibraryListUrl(String libraryListUrl) {
        this.libraryListUrl = libraryListUrl;
    }

    public String getDynamicLibraryUrl() {
        return dynamicLibraryUrl;
    }

    public void setDynamicLibraryUrl(String dynamicLibraryUrl) {
        this.dynamicLibraryUrl = dynamicLibraryUrl;
    }

    public String getAddLibraryDate() {
        return addLibraryDate;
    }

    public void setAddLibraryDate(String addLibraryDate) {
        this.addLibraryDate = addLibraryDate;
    }
}
