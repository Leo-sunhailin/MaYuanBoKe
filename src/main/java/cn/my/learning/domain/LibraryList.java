package cn.my.learning.domain;


import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ML on 2017/03/12.
 * 目录表
 */
@Document(collection = "my_article")
public class LibraryList implements Serializable {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id;    //id

    private String libraryName;  //类型名

    private String articleTitle;  //文章标题

    private String libraryCode;  //类型编号

    private String articleUrl;  //文章链接

    private String publisherAccount;  //作者账号

    private String publisher;  //作者名

    private String articleDate;  //发布时间

    private String articleCode;  //文章内容编号

    private String article;  //文章内容

    private String articleTag; //文章标签

    private String articleOrigin; //文章来源

    private String articleMD5; //文章内容MD5

    private String articleTitleMD5; //文章标题MD5

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public String getPublisherAccount() {
        return publisherAccount;
    }

    public void setPublisherAccount(String publisherAccount) {
        this.publisherAccount = publisherAccount;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getArticleDate() {
        return articleDate;
    }

    public void setArticleDate(String articleDate) {
        this.articleDate = articleDate;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getArticleTag() {
        return articleTag;
    }

    public void setArticleTag(String articleTag) {
        this.articleTag = articleTag;
    }

    public String getArticleOrigin() {
        return articleOrigin;
    }

    public void setArticleOrigin(String articleOrigin) {
        this.articleOrigin = articleOrigin;
    }

    public String getArticleMD5() {
        return articleMD5;
    }

    public void setArticleMD5(String articleMD5) {
        this.articleMD5 = articleMD5;
    }

    public String getArticleTitleMD5() {
        return articleTitleMD5;
    }

    public void setArticleTitleMD5(String articleTitleMD5) {
        this.articleTitleMD5 = articleTitleMD5;
    }
}
