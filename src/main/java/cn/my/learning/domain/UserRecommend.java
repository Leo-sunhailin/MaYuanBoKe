package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Leo on 2017/4/19.
 * 用户推荐表
 */
@Document(collection = "userRecommend")
public class UserRecommend implements Serializable {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id;  //id

    private String nowTime;

    private List<String> recommendList;

    private String account;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
    }

    public List<String> getRecommendList() {
        return recommendList;
    }

    public void setRecommendList(List<String> recommendList) {
        this.recommendList = recommendList;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
