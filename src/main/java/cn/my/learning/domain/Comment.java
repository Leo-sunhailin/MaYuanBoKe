package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Leo on 2017/4/21.
 * 文章评论表
 */
@Document(collection = "article_comment")
public class Comment implements Serializable {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id;

    private String articleCode;

    private String publisherAccount;

    private String commentTime;

    private List<Map<String, String>> comment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getPublisherAccount() {
        return publisherAccount;
    }

    public void setPublisherAccount(String publisherAccount) {
        this.publisherAccount = publisherAccount;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public List<Map<String, String>> getComment() {
        return comment;
    }

    public void setComment(List<Map<String, String>> comment) {
        this.comment = comment;
    }
}
