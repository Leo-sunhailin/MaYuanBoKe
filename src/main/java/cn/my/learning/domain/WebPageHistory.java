package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Leo on 2017/3/29.
 * 用户访问记录表
 */
@Entity
@Document(collection = "pageHistory")
public class WebPageHistory {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id; //ID

    private String SessionId; //SessionID

    private String PageUri; //URI

    private String RemoteAddr; //访问的IP地址

    private String RemoteAddrURL; //访问的IP地址的URL

    private String UserAgent; //UserAgent

    private String VisitTime; //访问时间

    private String Username; //访问的用户

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getPageUri() {
        return PageUri;
    }

    public void setPageUri(String pageUri) {
        PageUri = pageUri;
    }

    public String getRemoteAddr() {
        return RemoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        RemoteAddr = remoteAddr;
    }

    public String getRemoteAddrURL() {
        return RemoteAddrURL;
    }

    public void setRemoteAddrURL(String remoteAddrURL) {
        RemoteAddrURL = remoteAddrURL;
    }

    public String getUserAgent() {
        return UserAgent;
    }

    public void setUserAgent(String userAgent) {
        UserAgent = userAgent;
    }

    public String getVisitTime() {
        return VisitTime;
    }

    public void setVisitTime(String visitTime) {
        VisitTime = visitTime;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public WebPageHistory() {
    }
}
