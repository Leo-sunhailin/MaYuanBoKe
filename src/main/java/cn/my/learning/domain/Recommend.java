package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Leo on 2017/4/17.
 * 随机推荐表
 */
@Document(collection = "recommend")
public class Recommend implements Serializable {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id;  //id

    private List<String> recommendList;

    private String nowTime;

    private String account;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getRecommendList() {
        return recommendList;
    }

    public void setRecommendList(List<String> recommendList) {
        this.recommendList = recommendList;
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
