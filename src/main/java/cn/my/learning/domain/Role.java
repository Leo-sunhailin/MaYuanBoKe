package cn.my.learning.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Created by Leo on 2017/5/3.
 */
public class Role extends AbstractPersistable<Long> {

    private static final long serialVersionUID = -856234002396786101L;

    @Enumerated(EnumType.STRING)
    private RoleType roleType;
}
