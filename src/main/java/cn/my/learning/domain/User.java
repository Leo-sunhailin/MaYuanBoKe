package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/12/1.
 * 系统用户信息表
 */
@Document(collection = "my_user")
public class User implements Serializable {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id; //id

    private String account; //账户名

    private String username; //用户名

    private String signature; //个性签名

    private String password; //密码

    private String sex; //性别

    private String phone; //联系方式

    private String homePath; //住址

    private String headImg; //头像

    private String email; //邮箱

    private String registeredDate; //注册时间

    private List<String> articleCollection; //收藏的文章

    private List<String> followType; //关注的类型

    private String recentLoginTime; //最近登录时间

    private String recentLoginAddress; //最近登录地点(IP)

    private boolean isLoginNow; //是否在线

    private String sessionId; //SessionID

    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHomePath() {
        return homePath;
    }

    public void setHomePath(String homePath) {
        this.homePath = homePath;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(String registeredDate) {
        this.registeredDate = registeredDate;
    }

    public List<String> getArticleCollection() {
        return articleCollection;
    }

    public void setArticleCollection(List<String> articleCollection) {
        this.articleCollection = articleCollection;
    }

    public List<String> getFollowType() {
        return followType;
    }

    public void setFollowType(List<String> followType) {
        this.followType = followType;
    }

    public String getRecentLoginTime() {
        return recentLoginTime;
    }

    public void setRecentLoginTime(String recentLoginTime) {
        this.recentLoginTime = recentLoginTime;
    }

    public String getRecentLoginAddress() {
        return recentLoginAddress;
    }

    public void setRecentLoginAddress(String recentLoginAddress) {
        this.recentLoginAddress = recentLoginAddress;
    }

    public boolean isLoginNow() {
        return isLoginNow;
    }

    public void setLoginNow(boolean loginNow) {
        isLoginNow = loginNow;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
