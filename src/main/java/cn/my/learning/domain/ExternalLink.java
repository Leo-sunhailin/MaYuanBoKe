package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Administrator on 2017/4/5.
 * 外链表
 */
@Entity
@Document(collection = "ex_library")
public class ExternalLink implements Serializable {

    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id;  //id

    private String libraryName;  //类型名

    private String libraryIntro;  //类型简介

    private String libraryCode;  //编号

    private String libraryType; //分类

    private String libraryImg; //类型图片

    private String libraryListUrl; //类型目录链接

    private String addLibraryDate; //类型添加时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibraryIntro() {
        return libraryIntro;
    }

    public void setLibraryIntro(String libraryIntro) {
        this.libraryIntro = libraryIntro;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getLibraryType() {
        return libraryType;
    }

    public void setLibraryType(String libraryType) {
        this.libraryType = libraryType;
    }

    public String getLibraryImg() {
        return libraryImg;
    }

    public void setLibraryImg(String libraryImg) {
        this.libraryImg = libraryImg;
    }

    public String getLibraryListUrl() {
        return libraryListUrl;
    }

    public void setLibraryListUrl(String libraryListUrl) {
        this.libraryListUrl = libraryListUrl;
    }

    public String getAddLibraryDate() {
        return addLibraryDate;
    }

    public void setAddLibraryDate(String addLibraryDate) {
        this.addLibraryDate = addLibraryDate;
    }
}
