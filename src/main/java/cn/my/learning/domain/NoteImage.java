package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Leo on 2017/3/23.
 * 图片信息表
 */

@Entity
@Document(collection = "Image")
public class NoteImage implements Serializable {

    @Id
    @NotNull
    //提交图片的UUID
    private String uploadImgUUID;

    @NotNull
    //提交图片的名字
    private String uploadImgName;

    @NotNull
    //提交图片的绝对路径(服务器)
    private String uploadImgAbsolutePath;

    @NotNull
    //提交图片的格式
    private String uploadImgType;

    @NotNull
    //提交时间
    private String uploadTime;

    @NotNull
    //提交文库用户的用户名
    private String pubName;

    @NotNull
    //是否使用该图片在最终提交或者修改的文章中
    private boolean IsUse;

    //GETTER & SETTER
    public String getUploadImgUUID() {
        return uploadImgUUID;
    }

    public void setUploadImgUUID(String uploadImgUUID) {
        this.uploadImgUUID = uploadImgUUID;
    }

    public String getUploadImgName() {
        return uploadImgName;
    }

    public void setUploadImgName(String uploadImgName) {
        this.uploadImgName = uploadImgName;
    }

    public String getUploadImgAbsolutePath() {
        return uploadImgAbsolutePath;
    }

    public void setUploadImgAbsolutePath(String uploadImgAbsolutePath) {
        this.uploadImgAbsolutePath = uploadImgAbsolutePath;
    }

    public String getUploadImgType() {
        return uploadImgType;
    }

    public void setUploadImgType(String uploadImgType) {
        this.uploadImgType = uploadImgType;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(String uploadTime) {
        this.uploadTime = uploadTime;
    }

    public String getPubName() {
        return pubName;
    }

    public void setPubName(String pubName) {
        this.pubName = pubName;
    }

    public boolean isUse() {
        return IsUse;
    }

    public void setUse(boolean use) {
        IsUse = use;
    }


    //构造方法（无参数）
    public NoteImage() {}

    //构造方法（带参数）
    public NoteImage(String uploadImgUUID, String uploadImgName, String uploadImgAbsolutePath, String uploadImgType, String uploadTime, String pubName, boolean isUse) {
        this.uploadImgUUID = uploadImgUUID;
        this.uploadImgName = uploadImgName;
        this.uploadImgAbsolutePath = uploadImgAbsolutePath;
        this.uploadImgType = uploadImgType;
        this.uploadTime = uploadTime;
        this.pubName = pubName;
        IsUse = isUse;
    }
}
