package cn.my.learning.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Administrator on 2017/4/15.
 * 外链列表实体类
 */
@Entity
@Document(collection = "ex_articleList")
public class ExternalLinkList {
    @Id
    @GeneratedValue(generator="UUID2_GENERATOR")
    private String id;    //id

    private String libraryName;  //类型名

    private String articleTitle;  //文章标题

    private String libraryCode;  //类型编号

    private String articleUrl;  //文章链接

    private String publisher;  //作者名

    private String addArticleDate;  //发布时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getLibraryCode() {
        return libraryCode;
    }

    public void setLibraryCode(String libraryCode) {
        this.libraryCode = libraryCode;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAddArticleDate() {
        return addArticleDate;
    }

    public void setAddArticleDate(String addArticleDate) {
        this.addArticleDate = addArticleDate;
    }
}
