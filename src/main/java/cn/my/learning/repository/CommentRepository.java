package cn.my.learning.repository;

import cn.my.learning.domain.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by Leo on 2017/4/21.
 * 评论数据库层
 */
public interface CommentRepository extends MongoRepository<Comment, String> {

    /* 获取articleCode对应的评论数 */
    Page<Comment> findByArticleCode(String ArticleCode, Pageable pageable);

    //不分页
    List<Comment> findByArticleCode(String ArticleCode);

}
