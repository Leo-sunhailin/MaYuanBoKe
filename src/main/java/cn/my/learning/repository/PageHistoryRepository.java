package cn.my.learning.repository;

import cn.my.learning.domain.WebPageHistory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Leo on 2017/3/29.
 * 浏览轨迹历史记录数据库层
 */
@Repository
public interface PageHistoryRepository extends MongoRepository<WebPageHistory, String> {

    List<WebPageHistory> findBySessionIdIsNull();
}
