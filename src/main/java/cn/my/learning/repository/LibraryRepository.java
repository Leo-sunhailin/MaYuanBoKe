package cn.my.learning.repository;

import cn.my.learning.domain.Library;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ML on 2017/3/11.
 * 文章数据库层
 */
@Repository
public interface LibraryRepository extends PagingAndSortingRepository<Library, String> {

    Page<Library> findAll(Pageable pageable);

    Page<Library> findBylibraryType(Pageable pageable,String category);

    List<Library> findBylibraryCode(String libraryCode);

}

