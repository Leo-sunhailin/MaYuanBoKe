package cn.my.learning.repository;

import cn.my.learning.domain.NoteImage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Leo on 2017/3/24.
 * 图片数据库层
 */
public interface ImageRepository extends MongoRepository<NoteImage, String>{
    NoteImage insert(NoteImage image);
}
