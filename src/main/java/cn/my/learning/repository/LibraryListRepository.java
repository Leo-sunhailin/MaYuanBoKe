package cn.my.learning.repository;

import cn.my.learning.domain.LibraryList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/3/16.
 * 文章列表数据库层
 */
@Repository
public interface LibraryListRepository extends MongoRepository<LibraryList, String> {

    Page<LibraryList> findBylibraryCode(String libraryCode, Pageable pageable);

    List<LibraryList> findByarticleCode(String articleCode);

    Page<LibraryList> findBypublisherAccount(String publisherAccount, Pageable pageable);

    Page<LibraryList> findByArticleTitleLike(String articleTitle, Pageable pageable);
}
