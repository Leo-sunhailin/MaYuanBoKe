package cn.my.learning.repository;

import cn.my.learning.domain.ExternalLink;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/4/10.
 * 外链数据库层
 */
@Repository
public interface ExternalLinkRepository extends MongoRepository<ExternalLink, String>{

    /* 获取所有外链 */
    Page<ExternalLink> findAll(Pageable pageable);

    /* 获取对应libraryType的外链 */
    Page<ExternalLink> findBylibraryType(String libraryType,Pageable pageable);
}
