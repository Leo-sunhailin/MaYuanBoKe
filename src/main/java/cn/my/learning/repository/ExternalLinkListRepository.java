package cn.my.learning.repository;

import cn.my.learning.domain.ExternalLinkList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/4/16.
 * 外链列表数据库层
 */
@Repository
public interface ExternalLinkListRepository  extends MongoRepository<ExternalLinkList, String> {

    Page<ExternalLinkList> findBylibraryCode(String libraryCode, Pageable pageable);

    /* 获取对应libraryName的外链 */
    Page<ExternalLinkList> findByArticleTitleLike(String articleTitle, Pageable pageable);
}
