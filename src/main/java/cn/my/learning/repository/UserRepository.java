package cn.my.learning.repository;

import cn.my.learning.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ml on 2017/3/30.
 * 用户信息数据库层
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

     /**
      * 查找数据库是否存在此账号和密码
      * @param account 账号
      * @param password 密码
      * @return
      */
     List<User> findByAccountAndPassword(String account,String password);

     /**
      * 通过账号查找用户信息
      * @param account
      * @return
      */
     List<User> findByAccount(String account);

     User findUserByAccountAndPassword(String account,String password);

     User findUsernameByAccount(String account);
}
