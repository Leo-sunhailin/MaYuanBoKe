package cn.my.learning.repository;

import cn.my.learning.domain.LibraryList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/3/25.
 * 文章详情数据库层
 */
@Repository
public interface LibraryDetailRepository extends MongoRepository<LibraryList, String> {

    List<LibraryList> findByPublisherAndArticleCode(String publisher, String articleUrlSuffix);
}
