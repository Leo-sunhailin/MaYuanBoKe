package cn.my.learning.service;

import cn.my.learning.domain.NoteImage;
import cn.my.learning.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by Leo on 2017/4/12.
 * 图片管理实现服务层
 */
@Repository
public class ImageServiceImpl {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 删除未使用图片
     * @return
     */
    public void deleteUnUseImage(){
        mongoTemplate.remove(new Query(Criteria.where("IsUse").is(false)), NoteImage.class);
    }
}
