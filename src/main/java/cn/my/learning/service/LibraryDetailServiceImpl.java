package cn.my.learning.service;

import cn.my.learning.domain.LibraryList;
import cn.my.learning.domain.NoteImage;
import cn.my.learning.repository.LibraryDetailRepository;
import cn.my.learning.util.RegStringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/25.
 * 文章详情服务层
 */
@Repository
public class LibraryDetailServiceImpl {

    @Autowired
    private LibraryDetailRepository libraryDetailRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    private static final Logger logger = LoggerFactory.getLogger(LibraryDetailServiceImpl.class);

    private RegStringUtil reg = new RegStringUtil();

    /**
     * 添加一篇文章
     *
     * @param libraryList
     */
    public boolean addLibraryDetail(LibraryList libraryList) {
        libraryList.setArticleDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        List<String> returnRegImg = reg.getString(libraryList.getArticle(), libraryList.getPublisherAccount(), RegStringUtil.regx);
        libraryDetailRepository.save(libraryList);

        //循环把文章中有的Img设为True，默认IsUse是False
        for (String Img : returnRegImg){
            mongoTemplate.upsert(new Query(Criteria.where("uploadImgUUID").is(Img)), new Update().set("IsUse", true), NoteImage.class);
        }

        //获得数据库中该用户没用过的Img
        List<NoteImage> deleteImgList = mongoTemplate.find(new Query(Criteria.where("pubName").is(libraryList.getPublisherAccount()).and("IsUse").is(false)),NoteImage.class);

        //for循环获得绝对路径，对这些图片进行删除
        for (NoteImage deleteImg : deleteImgList) {
            String deletePath = deleteImg.getUploadImgAbsolutePath();
            File delete_file = new File(deletePath);
            if (delete_file.delete()) {
                logger.info(deleteImg.getUploadImgName() + "删除成功!");
            } else {
                logger.info(deleteImg.getUploadImgName() + "删除失败!");
            }
        }

        //删除该用户Img数据库记录中图片为IsUsede
        mongoTemplate.remove(new Query(Criteria.where("pubName").is(libraryList.getPublisherAccount()).andOperator(Criteria.where("IsUse").is(false))),NoteImage.class);
        return true;
    }

    /**
     * 这个方法是为了重复提交而写，后续会对代码进行改写（暂时使用）
     *
     * @param libraryList
     * @param id
     */
    public boolean updateLibraryDetail(LibraryList libraryList, String id){
        libraryList.setArticleDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        libraryDetailRepository.delete(id);
        libraryDetailRepository.save(libraryList);
        return true;
    }

    /**
     * 通过id去查询文章详情
     * @param id
     * @return
     */
    public String findByIdLibaryDetail(String id){
        LibraryList libraryList = libraryDetailRepository.findOne(id);
        return libraryList.getId();
    }
}
