package cn.my.learning.service;

import cn.my.learning.domain.Library;
import cn.my.learning.domain.LibraryList;
import cn.my.learning.repository.LibraryListRepository;
import cn.my.learning.util.PageableTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.my.learning.repository.LibraryRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ML on 2017/3/11.
 * 文章服务层
 */
@Repository
public class LibraryServiceImpl {

    @Autowired
    private LibraryRepository libraryRepository;

    @Autowired
    private LibraryListRepository libraryListRepository;

    /**
     * 查找对应libraryType的书库类别
     * @param libraryType
     * @param current
     * @param size
     * @return
     */
    public Page<Library> findLibraryType(String libraryType,int current, int size) {
        Pageable pageable = PageableTools.addLibraryDateSortForDesc(current, size);
        if(libraryType.equals("")){
            return libraryRepository.findAll(pageable);
        }else{
            return libraryRepository.findBylibraryType(pageable,libraryType);
        }

    }

    /**
     * 查找相应书库类别的目录
     * libraryCode 查询条件
     * current 页数
     * size 一页数据数
     * @return
     */
    public Page<LibraryList> findLibraryCode_list(String libraryCode,int current, int size) {
        Pageable pageable = PageableTools.addTimeSortForDescMyArc(current, size);
        return libraryListRepository.findBylibraryCode(libraryCode,pageable);
    }

    /**
     * 查找相应书库类别的数据
     * libraryCode 查询条件
     * @return
     */
    public List<Library> findLibraryCode(String libraryCode) {
        return libraryRepository.findBylibraryCode(libraryCode);
    }

    /**
     * 查找相应articleCode的的文章内容
     * articleCode 查询条件
     * @return
     */
    public List<LibraryList> findArticleCode(String articleCode) {
        return libraryListRepository.findByarticleCode(articleCode);
    }

    /**
     * 模糊查询my_article的libraryName
     * @param articleTitle
     * @param current
     * @param size
     * @return
     */
    public Page<LibraryList> findLibraryListByArticleTitle(String articleTitle, Integer current, Integer size){
        Pageable pageable = PageableTools.addTimeSortForDescAndPage(current, size);
        return libraryListRepository.findByArticleTitleLike(articleTitle, pageable);
    }
}
