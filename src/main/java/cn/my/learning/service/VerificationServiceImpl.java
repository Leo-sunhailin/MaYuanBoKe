package cn.my.learning.service;

import cn.my.learning.domain.Verification;
import cn.my.learning.util.EmailUtil;
import com.mongodb.WriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Leo on 2017/4/9.
 * 验证码增删改查实现服务层
 */
@Repository
public class VerificationServiceImpl {

    @Autowired
    private MongoTemplate mongoTemplate;

    EmailUtil emailUtil = new EmailUtil();

    /**
     * 添加一条证书记录
     * @param account
     * @param userEmailAddr
     * @param verificationString
     * @return
     * @throws ParseException
     */
    public boolean addVerification(String account, String userEmailAddr, String verificationString) throws ParseException {
        //获得生效时间以及失效时间
        String start_time = emailUtil.getStartTime();
        String end_time = emailUtil.getOutdateTime(start_time);

        //实例化证书对象
        Verification verification = new Verification();

        //set证书对象
        verification.setUserAccount(account);
        verification.setUserEmailAddress(userEmailAddr);
        verification.setVerification(verificationString);
        verification.setStart_time(start_time);
        verification.setEnd_time(end_time);
        verification.setIsExpired(true);

        //保存到数据库
        mongoTemplate.save(verification);
        return true;
    }

    /**
     * 通过邮件地址和账户找验证码对象
     * @param EmailAddress
     * @param account
     * @return
     */
    public List<Verification> findByEmailAddressAndAccount(String EmailAddress, String account){
        Criteria criteria = new Criteria();
        criteria.andOperator(Criteria.where("userEmailAddress").is(EmailAddress), Criteria.where("userAccount").is(account), Criteria.where("isExpired").is(true));
        return mongoTemplate.find(new Query(criteria), Verification.class);
    }

    /**
     * 修改验证码状态
     * @param randomString
     * @return
     */
    public boolean ChangeVerificationExpired(String randomString){
        WriteResult writeResult = mongoTemplate.updateFirst(new Query(Criteria.where("verification").is(randomString)), Update.update("isExpired", false), Verification.class);
        return writeResult.isUpdateOfExisting();
    }

    /**
     * 查找所有没失效的验证码
     */
    public List<Verification> findIsExpiredAll(){
        return mongoTemplate.find(new Query(Criteria.where("IsExpired").is(true)), Verification.class);
    }

    /**
     * 查找所有的验证码
     */
    public List<Verification> findAll(){
        return mongoTemplate.findAll(Verification.class);
    }

    /**
     * 删除失效的验证码（定时）
     */
    public boolean removeUnverifyVerification(Verification verification){
        WriteResult writeResult = mongoTemplate.remove(verification);
        return writeResult.isUpdateOfExisting();
    }
}
