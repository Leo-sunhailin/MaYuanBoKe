package cn.my.learning.service;

import cn.my.learning.domain.Recommend;
import cn.my.learning.domain.UserRecommend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Leo on 2017/4/17.
 * 推荐系统服务层
 */
@Repository
public class RecommendServiceImpl {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 返回所有推荐列表
     * @return
     */
    public List<String> getGuestRecommend(){
        Integer count = Math.toIntExact(mongoTemplate.getCollection("recommend").count());
        return mongoTemplate.findAll(Recommend.class).get(count - 1).getRecommendList();
    }

    /**
     * 返回登陆用户的推荐列表
     * @param userAccount
     * @return
     */
    public List<UserRecommend> getUserPersonalRecommend(String userAccount){
        return mongoTemplate.find(new Query(Criteria.where("account").is(userAccount)), UserRecommend.class);
    }
}
