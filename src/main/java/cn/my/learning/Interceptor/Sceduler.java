package cn.my.learning.Interceptor;

import cn.my.learning.domain.Recommend;
import cn.my.learning.domain.Verification;
import cn.my.learning.domain.WebPageHistory;
import cn.my.learning.service.ImageServiceImpl;
import cn.my.learning.service.VerificationServiceImpl;
import cn.my.learning.util.EmailUtil;
import com.mongodb.WriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Leo on 2017/4/12.
 * 定时任务
 */
@Component
public class Sceduler {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ImageServiceImpl imageService;

    @Autowired
    private VerificationServiceImpl verificationService;

    public final static long ONE_DAY = 24 * 60 * 60 * 1000;

    //每五分钟执行一次
    /**
     * 删除没使用的用户图片
     */
    @Scheduled(fixedRate = 5 * 60 * 1000)
    public void deleteUnUseImage(){
        imageService.deleteUnUseImage();
    }

    //每十分钟执行一次

    /**
     * 修改过期的验证码状态
     * @throws ParseException
     */
    @Scheduled(fixedRate = 10 * 60 * 1000)
    public void modifyVerification() throws ParseException {
        EmailUtil emailUtil = new EmailUtil();
        List<Verification> verificationList = verificationService.findAll();
        for (Verification verification : verificationList){
            emailUtil.detla_T(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())),verification.getEnd_time(), verification.getStart_time());
            verificationService.removeUnverifyVerification(verification);
        }
    }

    //每十分钟执行一次

    /**
     * 删除多余的游客推荐记录
     */
    @Scheduled(fixedRate = 10 * 60 * 1000)
    public void deleteGuestRecommendList(){
        Integer count = Math.toIntExact(mongoTemplate.getCollection("recommend").count());
        if (count > 5){
            List<Recommend> list = mongoTemplate.find(new Query().limit(count - 5), Recommend.class);
            for (Recommend recommend : list){
                mongoTemplate.remove(recommend);
            }
        }
    }

    //每十分钟执行一次
    /**
     *  删除SessionId为空的浏览轨迹
     */
    @Scheduled(fixedRate = 30 * 60 * 1000)
    public void deleteNoSessionID(){
        Query query = Query.query(Criteria.where("SessionId").is(""));
        WriteResult removeResult = mongoTemplate.remove(query, WebPageHistory.class);
        boolean IsDelete = removeResult.isUpdateOfExisting();
        System.out.println(IsDelete);
    }

    //用来删除此前在externalLink的多余图片
    /*
    @Scheduled(fixedRate = 60 * 1000)
    public void deleteFileName(){
        String path = "E:\\WEB\\MY\\src\\main\\resources\\static\\image\\externalLink\\";
        File f = new File(path);
        if (!f.exists()) {
            System.out.println(path + " not exists");
            return;
        }

        List<String> fileName_big = new ArrayList<>();
        File fa[] = f.listFiles();
        for (int i = 0; i < fa.length; i++) {
            File fs = fa[i];
            fileName_big.add(fs.getName());
        }
        System.out.println(fileName_big.size());
        System.out.println(fileName_big);

        List<String> fileName_correct = new ArrayList<>();
        List<ExternalLink> externalLinks = mongoTemplate.findAll(ExternalLink.class);
        for (ExternalLink externalLink : externalLinks){
            fileName_correct.add(externalLink.getLibraryCode() + ".png");
        }
        System.out.println(fileName_correct.size());
        System.out.println(fileName_correct);

        fileName_big.removeAll(fileName_correct);

        for (String fileName : fileName_big){
            File deleteFile = new File(path + fileName);
            deleteFile.delete();
        }

        System.out.println("Finish");
    }*/

}
