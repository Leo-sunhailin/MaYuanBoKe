package cn.my.learning.security;

/**
 * Created by Leo on 2017/5/21.
 * 获取跳转到登陆页面前的链接
 */
public class BeforeLoginRequest {

    public static String RequestUrl;

    public static String getRequestUrl() {
        return RequestUrl;
    }

    public static void setRequestUrl(String requestUrl) {
        RequestUrl = requestUrl;
    }
}
