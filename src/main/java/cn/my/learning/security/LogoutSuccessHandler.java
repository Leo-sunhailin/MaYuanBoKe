package cn.my.learning.security;

import cn.my.learning.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Leo on 2017/5/18.
 * 退出登录Handler
 */
@Component
public class LogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private UserServiceImpl userService;

    public LogoutSuccessHandler(){
        this.setUseReferer(true);
    }

    /**
     * 退出登录实现方法
     * @param request
     * @param response
     * @param authentication
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        super.onLogoutSuccess(request, response, authentication);
        String account = authentication.getPrincipal().toString();
        userService.LogoutStatus(account);
    }
}

