package cn.my.learning.web;

import cn.my.learning.Interceptor.WebPageConfig;
import cn.my.learning.domain.UserRecommend;
import cn.my.learning.service.RecommendServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Leo on 2017/4/17.
 * 推荐系统管理类
 */
@Controller
public class RecommendController {

    @Autowired
    private RecommendServiceImpl recommendService;

    @Autowired
    private static Logger logger = LoggerFactory.getLogger(RecommendController.class);

    @ApiOperation(value = "获取游客推荐", httpMethod = "GET", notes = "游客推荐系统")
    @RequestMapping(value = "getGuestRecommend")
    @ResponseBody
    public List<String> getGuestRecommend(){
        return recommendService.getGuestRecommend();
    }

    @ApiOperation(value = "获取用户推荐", httpMethod = "GET", notes = "用户推荐系统")
    @RequestMapping(value = "getUserPersonalRecommend")
    @ResponseBody
    public List<String> getUserPersonalRecommend(HttpServletRequest httpServletRequest){
        if (httpServletRequest.getRemoteUser() != null){
            List<UserRecommend> userPersonalRecommend = recommendService.getUserPersonalRecommend(httpServletRequest.getRemoteUser());
            if (userPersonalRecommend.size() == 0){
                logger.error("用户推荐失效，切换至游客推荐模式...");
                //假设返回用户推荐为空，则调用游客推荐
                return null;
            }
            return userPersonalRecommend.get(0).getRecommendList();
        }
        return null;
    }

}
