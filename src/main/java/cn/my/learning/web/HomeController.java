package cn.my.learning.web;

import cn.my.learning.domain.ExternalLinkList;
import cn.my.learning.domain.LibraryList;
import cn.my.learning.domain.User;
import cn.my.learning.repository.UserRepository;
import cn.my.learning.service.ExternalLinkServiceImpl;
import cn.my.learning.service.LibraryServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Created by ML on 2017/3/12.
 * 主页管理类
 */
@Controller
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExternalLinkServiceImpl externalLinkService;

    @Autowired
    private LibraryServiceImpl libraryService;

    /**
     * 跳转到主页
     * @return
     */
    @ApiOperation(value = "跳转到主页", httpMethod = "GET" ,notes = "跳转到主页")
    @RequestMapping(value = "/my_home",method = RequestMethod.GET)
    public String goHome(HttpServletRequest httpServletRequest){
        if (httpServletRequest.getRemoteUser() != null){
            User user = userRepository.findUsernameByAccount(httpServletRequest.getRemoteUser());
            httpServletRequest.setAttribute("Username", user.getUsername());
        }
        return "my_home";
    }

    @ApiOperation(value = "跳转到error页面", httpMethod = "GET" ,notes = "跳转到error页面")
    @RequestMapping(value = "/error",method = RequestMethod.GET)
    public String goError(){
        return "error";
    }

    @ApiOperation(value = "跳转到404页面", httpMethod = "GET" ,notes = "跳转到404页面")
    @RequestMapping(value = "/404",method = RequestMethod.GET)
    public String go404(){ return "redirect:/error"; }

    @ApiOperation(value = "默认跳转首页界面", httpMethod = "GET" ,notes = "默认跳转首页界面")
    @RequestMapping(value = "/")
    public String goIndex(HttpServletRequest httpServletRequest){
        if (httpServletRequest.getRemoteUser() != null){
            User user = userRepository.findUsernameByAccount(httpServletRequest.getRemoteUser());
            httpServletRequest.setAttribute("Username", user.getUsername());
        }
        return "my_home";
    }

    @ApiOperation(value = "跳转到搜索页面", httpMethod = "GET" ,notes = "跳转到搜索页面")
    @RequestMapping(value = "/researchPage")
    public String goResearchPage(HttpServletRequest httpServletRequest){
        if (httpServletRequest.getRemoteUser() != null){
            User user = userRepository.findUsernameByAccount(httpServletRequest.getRemoteUser());
            httpServletRequest.setAttribute("Username", user.getUsername());
        }
        return "researchPage";
    }

    @ApiOperation(value = "全局搜索", httpMethod = "POST" ,notes = "全局搜索")
    @RequestMapping(value = "get_research")
    @ResponseBody
    public List<Object> allResearch(@RequestParam String researchText, @RequestParam String researchType, @RequestParam Integer current, @RequestParam Integer size){
        //初始化一个Object List(因为查询的实体类不同返回的都是对象List)
        List<Object> researchResults = new ArrayList<>();

        //数据库模糊查询语句(MongoTemplate的写法)
        //Query query_search_library = new Query(Criteria.where("articleTitle").regex("/*." + researchText + "/i")); //regex(".*?" + researchText + ".*"))
        //query_search_library.skip((current - 1) * size).limit(size);
        //List<ExternalLinkList> externalLinkLists = mongoTemplate.find(query_search_library, ExternalLinkList.class);

        if (Objects.equals(researchType, "外链")){
            Page<ExternalLinkList> externalLinkListAfterSearch = externalLinkService.findExternalLinkListByArticleTitle(researchText, current, size);
            researchResults.add(externalLinkListAfterSearch);
            return researchResults;
        }

        else if (Objects.equals(researchType, "码猿")){
            Page<LibraryList> libraryListAfterSearch = libraryService.findLibraryListByArticleTitle(researchText, current, size);
            researchResults.add(libraryListAfterSearch);
            return researchResults;
        }
        return null;
    }
}
