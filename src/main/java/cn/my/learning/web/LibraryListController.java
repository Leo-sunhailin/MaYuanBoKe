package cn.my.learning.web;

import cn.my.learning.domain.LibraryList;
import cn.my.learning.domain.User;
import cn.my.learning.repository.UserRepository;
import cn.my.learning.service.LibraryServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Administrator on 2017/3/16.
 * 文章列表管理类
 */
@Controller
@RequestMapping(value = "/libraryList")
public class LibraryListController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LibraryServiceImpl libraryService;

    @ApiOperation(value = "跳转到文库目录页面", httpMethod = "GET", notes = "跳转到文库目录页面")
    @RequestMapping(value = "/{libraryCode}")
    public String goLibraryList(HttpServletRequest httpServletRequest){
        Object securityContext = httpServletRequest.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
        if (securityContext != null){
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) securityContext;
            String userAccount = securityContextImpl.getAuthentication().getName();
            User user = userRepository.findUsernameByAccount(userAccount);
            httpServletRequest.setAttribute("Username", user.getUsername());
        }
        return "libraryList";
    }

    @ApiOperation(value = "跳转到文章详情页面", httpMethod = "GET", notes = "跳转到文章详情页面")
    @RequestMapping(value = "/{libraryCode}/{articleCode}")
    public String goLibraryDetail(HttpServletRequest httpServletRequest){
        Object securityContext = httpServletRequest.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
        if (securityContext != null){
            SecurityContextImpl securityContextImpl = (SecurityContextImpl) securityContext;
            User user = userRepository.findUsernameByAccount(securityContextImpl.getAuthentication().getName());
            httpServletRequest.setAttribute("Username", user.getUsername());
        }
        return "libraryDetail";
    }

    @ApiOperation(value = "获取所有对应libraryCode的文库目录", httpMethod = "GET", notes = "获取所有对应libraryCode的文库目录")
    @RequestMapping(value = "get_libraryList")
    @ResponseBody
    public Page<LibraryList> getLibrary(@RequestParam Integer currentPage,@RequestParam String libraryCode) {
        //构建分页信息
        return libraryService.findLibraryCode_list(libraryCode,currentPage, 10);
    }

    @ApiOperation(value = "获取对应articleUrlSuffix的文章内容", httpMethod = "GET", notes = "获取对应articleUrlSuffix的文章内容")
    @RequestMapping(value = "get_libraryDetail")
    @ResponseBody
    public List<LibraryList> getLibraryDetail(@RequestParam String articleCode) {
        return libraryService.findArticleCode(articleCode);
    }
}
