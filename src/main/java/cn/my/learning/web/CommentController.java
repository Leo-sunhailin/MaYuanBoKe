package cn.my.learning.web;

import cn.my.learning.Interceptor.WebPageConfig;
import cn.my.learning.domain.Comment;
import cn.my.learning.domain.User;
import cn.my.learning.repository.UserRepository;
import cn.my.learning.service.CommentServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Leo on 2017/4/21.
 * 评论管理类
 */
@Controller
public class CommentController {

    @Autowired
    private CommentServiceImpl commentService;

    @ApiOperation(value = "获取文章的评论", httpMethod = "GET", notes = "获取文章的评论")
    @RequestMapping("getArticleComment")
    @ResponseBody
    public Page<Comment> getArticleComment(@RequestParam String articleCode,
                                           @RequestParam Integer current,
                                           @RequestParam Integer size)
    {
        return commentService.getLibraryComment(articleCode, current, size);
    }

    @ApiOperation(value = "添加文章的评论", httpMethod = "POST", notes = "添加文章的评论")
    @RequestMapping("addArticleComment")
    @ResponseBody
    public boolean addArticleComment(HttpServletRequest httpServletRequest,
                                     @RequestParam String commentToWhoAccount,
                                     @RequestParam String commentText,
                                     @RequestParam String articleCode)
    {
        String commentPublisher = httpServletRequest.getRemoteUser();
        if (commentPublisher != null){
            //评论人 + 被评论人 + 评论内容 + 文章号
            System.out.println(commentPublisher + "####" + commentToWhoAccount + "####" + commentText + "####" + articleCode);
            return commentService.addLibraryComment(commentToWhoAccount, articleCode, commentToWhoAccount, commentPublisher, commentText);
        }
        return false;
    }
}
