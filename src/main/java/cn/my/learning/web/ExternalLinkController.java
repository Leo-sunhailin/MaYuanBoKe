package cn.my.learning.web;

import cn.my.learning.domain.ExternalLink;
import cn.my.learning.domain.ExternalLinkList;
import cn.my.learning.domain.User;
import cn.my.learning.repository.UserRepository;
import cn.my.learning.service.ExternalLinkServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2017/4/6.
 * 外链管理类
 */
@Controller
public class ExternalLinkController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExternalLinkServiceImpl externalLinkService;

    @ApiOperation(value = "跳转到外链界面",httpMethod = "GET" ,notes = "跳转到外链界面")
    @RequestMapping(value = "/externalLink",method = RequestMethod.GET)
    public String goExternalLink(HttpServletRequest httpServletRequest){
        if (httpServletRequest.getRemoteUser() != null){
            User user = userRepository.findUsernameByAccount(httpServletRequest.getRemoteUser());
            httpServletRequest.setAttribute("Username", user.getUsername());
        }
        return "externalLink";
    }

    @ApiOperation(value = "跳转到外链界面",httpMethod = "GET" ,notes = "跳转到外链界面")
    @RequestMapping(value = "/externalLink/{libraryCode}",method = RequestMethod.GET)
    public String goExternalLinkList(@PathVariable("libraryCode") String libraryCode, HttpServletRequest httpServletRequest){
        if (httpServletRequest.getRemoteUser() != null){
            User user = userRepository.findUsernameByAccount(httpServletRequest.getRemoteUser());
            httpServletRequest.setAttribute("Username", user.getUsername());
        }
        return "externalLinkList";
    }

    @ApiOperation(value = "跳转到外链", notes = "跳转到外链")
    @RequestMapping(value = "/articleDetails",method = RequestMethod.GET)
    public void jumpToExternalLink(String target,HttpServletResponse httpServletResponse) throws IOException {
        System.out.println(target);
        httpServletResponse.sendRedirect(target);
    }

    @ApiOperation(value = "查询所有的外链记录", httpMethod = "GET",  notes = "返回所有外链记录给前台页面")
    @RequestMapping(value = "get_all_external_link")
    @ResponseBody
    public Page<ExternalLink> findAllExternalLink(@RequestParam String libraryType,@RequestParam Integer currentPage,@RequestParam Integer size){
        return externalLinkService.findAllExternalLink(libraryType,currentPage,size);
    }

    @ApiOperation(value = "通过libraryCode查询外链记录", httpMethod = "GET",  notes = "返回查询到的外链记录给前台页面")
    @RequestMapping(value = "get_external_link_byLibraryCode")
    @ResponseBody
    public List<ExternalLink> findByLibraryCode(@RequestParam String libraryCode){
        return externalLinkService.findByLibraryCode(libraryCode);
    }

    @ApiOperation(value = "通过libraryCode查询外链目录", httpMethod = "GET",  notes = "返回查询到的外链目录给前台页面")
    @RequestMapping(value = "get_externalLinkList")
    @ResponseBody
    public Page<ExternalLinkList> findByLibraryCodeList(@RequestParam String libraryCode, @RequestParam Integer current,@RequestParam Integer size){
        return externalLinkService.findByLibraryCodeList(libraryCode, current, size);
    }


}
