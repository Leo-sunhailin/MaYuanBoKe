package cn.my.learning.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Leo on 2017/4/1.
 * 正则匹配工具类
 */
public class RegStringUtil {

    private static final Logger logger = LoggerFactory.getLogger(RegStringUtil.class);

    public static final String regx = "/[a-zA-Z0-9]+";
    /**
     * 获取查询的字符串
     * 将匹配的字符串取出
     */
    public List<String> getString(String str, String Username, String regx) {
        //1.将正在表达式封装成对象Patten 类来实现
        Pattern pattern = Pattern.compile(Username + regx);
        logger.info(pattern.toString());
        //2.将字符串和正则表达式相关联
        Matcher matcher = pattern.matcher(str);
        logger.info(matcher.toString());

        List<String> matcherList = new ArrayList<>();
        //查找符合规则的子串
        while(matcher.find()){
            //获取 字符串
            String afterMatch = matcher.group().toString().replace(Username + "/", "");
            logger.info(afterMatch);
            matcherList.add(afterMatch);
        }
        return matcherList;
    }
}
