package cn.my.learning.util;

import java.util.UUID;

/**
 * UUID工具类
 */
public class UuidUtil {

	//获取一个UUID
	public static String get32UUID() {
		return UUID.randomUUID().toString().trim().replaceAll("-", "");
	}

}

