package cn.my.learning.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * 分页工具类
 * Created by WangZK on 2016/12/8.
 */
public class PageableTools {

    /**
     * 获取基础分页对象
     * @param page  获取第几页
     * @param size  每页条数
     * @return
     */
    public static Pageable basicPage(Integer page, Integer size) {
        page = (page==null || page<0)?0:page;
        size = (size==null || size<=0)?10:size;
        Pageable pageable = new PageRequest(page, size);
        return pageable;
    }

    /**
     * 获取基础分页对象，每页条数默认15条
     * @param page 获取第几页
     * @return
     */
    public static Pageable basicPage(Integer page) {
        return basicPage(page, 0);
    }

    /**
     * 获取按添加时间降序排序和分页的对象
     *
     * @param page
     * @param size
     * @return
     */
    public static Pageable addTimeSortForDescMyArc(Integer page, Integer size) {
        Sort sort = SortTools.addTimeSortForDescMyArc();
        page = (page == null || page < 0) ? 0 : page;
        size = (size == null || size <= 0) ? 10 : size;
        Pageable pageable = new PageRequest(page, size, sort);
        return pageable;
    }

    /**
     * 按添加时间降序排序
     *
     * @return
     */
    public static Pageable addLibraryDateSortForDesc(Integer page, Integer size) {
        Sort sort = SortTools.addLibraryDateSortForDesc();
        page = (page == null || page < 0) ? 0 : page;
        size = (size == null || size <= 0) ? 10 : size;
        Pageable pageable = new PageRequest(page, size, sort);
        return pageable;
    }


    /**
     * 获取按删除时间降序排序和分页的对象
     *
     * @param page
     * @param size
     * @return
     */
    public static Pageable deletedTimeSortForDescAndPage(Integer page, Integer size) {
        Sort sort = SortTools.deletedTimeSortForDesc();
        page = (page == null || page < 0) ? 0 : page;
        size = (size == null || size <= 0) ? 10 : size;
        Pageable pageable = new PageRequest(page, size, sort);
        return pageable;
    }

    /**
     * 普通分页
     * @param page
     * @param size
     * @return
     */
    public static Pageable normalSort(Integer page, Integer size){
        page = (page == null || page < 0) ? 0 : page;
        size = (size == null || size <= 0) ? 10 : size;
        return new PageRequest(page, size);
    }

    /**
     * 按照libraryName的添加时间排序
     * @param page
     * @param size
     * @return
     */
    public static Pageable addTimeSortForDescAndPage(Integer page, Integer size){
        Sort sort = SortTools.basicSort("DESC", "articleDate");
        page = (page == null || page < 0) ? 0 : page;
        size = (size == null || size <= 0) ? 10 : size;
        Pageable pageable = new PageRequest(page, size, sort);
        return pageable;
    }

    /**
     * 按评论时间升序排序（最新评论在前）
     * @param page
     * @param size
     * @return
     */
    public static Pageable commentSort(Integer page, Integer size) {
        Sort sort = SortTools.basicSort("ASC", "commentTime");
        page = (page == null || page < 0) ? 0 : page;
        size = (size == null || size <= 0) ? 10 : size;
        Pageable pageable = new PageRequest(page, size, sort);
        return pageable;
    }
}
