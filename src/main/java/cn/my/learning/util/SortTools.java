package cn.my.learning.util;

import org.springframework.data.domain.Sort;

/**
 * Created by WangZK on 2017/2/13.
 * 分页工具类
 */
public class SortTools {


    /**
     * 获取基础的排序对象
     *
     * @param orderType
     * @param orderField
     * @return
     */
    public static Sort basicSort(String orderType, String orderField) {
        Sort sort = new Sort(Sort.Direction.fromString(orderType), orderField);
        return sort;
    }

    /**
     * 按添加时间降序排序
     *
     * @return
     */
    public static Sort addTimeSortForDesc() {
        return basicSort("DESC", "articleTime");
    }

    /**
     * 按添加时间降序排序
     *
     * @return
     */
    public static Sort addLibraryDateSortForDesc() {
        return basicSort("DESC", "addLibraryDate");
    }

    /**
     * 按删除时间降序排序
     *
     * @return
     */
    public static Sort deletedTimeSortForDesc() {
        return basicSort("DESC", "deletedTime");
    }

    /**
     * 按添加时间降序排序
     * my_article
     *
     * @return
     */
    public static Sort addTimeSortForDescMyArc(){ return basicSort("DESC", "articleDate"); }
}
