package cn.my.learning.util;

import cn.my.learning.Interceptor.WebPageConfig;
import cn.my.learning.domain.User;
import cn.my.learning.repository.UserRepository;
import cn.my.learning.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Leo on 2017/4/3.
 * 登录检查工具类
 */
public class IsLoginUtil {

    //普通用户
    public String IsLogin(HttpServletRequest httpServletRequest){

        //获取用户名
        String userFolderName = "未登录用户";

        Object object = httpServletRequest.getRemoteUser();
        if (object != null){
            userFolderName = object.toString();
        }
        return userFolderName;
    }

}
