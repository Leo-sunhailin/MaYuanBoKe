package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.Library;
import cn.my.learning.repository.LibraryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 测试书库（已完成）
 * Created by Administrator on 2017/4/10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestLibrary {
   /* private String libraryName;  //类型名
    private String libraryIntro;  //类型简介
    private String libraryCode;  //编号
    private String libraryImg; //类型图
    private String libraryType; //类型分类
    private String libraryListUrl; //类型目录链接
    private String dynamicLibraryUrl; //动态图库链接
    private String addLibraryDate; //类型添加时间
*/
   @Autowired
    private LibraryRepository libraryRepository;
    @Test
    public void SaveLibrary(){
        Library library=new Library();
        library.setLibraryName("敏捷");
        library.setLibraryIntro("敏捷是以用户的需求进化为核心，采用迭代、循序渐进的方法进行软件开发、管理。它是针对传统的瀑布开发模式的弊端而产生的一种新的模式，目标是提高生产效率和响应能力。");
        library.setLibraryCode("agile");
        library.setLibraryImg("http://img.knowledge.csdn.net/upload/base/1471918993092_92.jpg");
        library.setLibraryType("软件工程");
        library.setLibraryListUrl("/libraryList/agile");
        library.setDynamicLibraryUrl("http://lib.csdn.net/base/agile/structure");
        Date date=new Date();
        library.setAddLibraryDate(date.toString());
        libraryRepository.save(library);
        assertThat(library.getId()).isNotNull();
    }
}
