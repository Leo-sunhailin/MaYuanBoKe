package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.ExternalLinkList;
import cn.my.learning.repository.ExternalLinkListRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Administrator on 2017/4/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestExternalLinkList {
/*    private String libraryName;  //类型名
    private String articleTitle;  //文章标题
    private String libraryCode;  //类型编号
    private String articleUrl;  //文章链接
    private String publisher;  //作者名
    private String articleDate;  //发布时间*/
    @Autowired
    private ExternalLinkListRepository externalLinkListRepository;
    @Test
    public void saveExternalLinkList(){
        ExternalLinkList externalLinkList=new ExternalLinkList();
        externalLinkList.setLibraryName("前端");
        externalLinkList.setArticleTitle("破解前端面试（80% 应聘者不及格系列）：从闭包说起");
        externalLinkList.setLibraryCode("lZAdrF1P");
        externalLinkList.setArticleUrl("https://juejin.im/post/58f1fa6a44d904006cf25d22");
        externalLinkList.setPublisher("王仕军");
        externalLinkList.setAddArticleDate(new SimpleDateFormat("yy-MM-dd HH-mm-ss").format(new Date()));
        externalLinkListRepository.save(externalLinkList);
        assertThat(externalLinkList.getId()).isNotNull();
    }
}
