package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.LibraryList;
import cn.my.learning.repository.LibraryListRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 测试用户的博客（已完成）
 * Created by Administrator on 2017/4/10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestLibraryList {
    @Autowired
    private LibraryListRepository libraryListRepository;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
/*
    private String articleOrigin; //文章来源
    private String articleMD5; //文章内容MD5
    private String articleTitleMD5; //文章标题MD5*/
    @Test
    public void SaveArticle(){
        LibraryList libraryList=new LibraryList();
        libraryList.setLibraryName("敏捷");
        libraryList.setArticleTitle("敏捷文章");
        libraryList.setLibraryCode("agile");
        libraryList.setArticleUrl("/libraryList/agile/prXK5YEaGk3rYRZ2");
        libraryList.setPublisherAccount("201406114235");
        libraryList.setPublisher("ZMagic");
        Date date=new Date();
        libraryList.setArticleDate(dateFormat.format(date));
        libraryList.setArticle("Spring Boot 项目旨在简化创建产品级的 Spring 应用和服务。你可通过它来选择不同的 Spring 平台。可创建独立的 Java 应用和 Web 应用，同时提供了命令行工具来允许 'spring scripts'.![aa](/image/uploadImage/201406114235/141044501dc14d46b16eea0a6edccf7e.jpg \"aa\")");
        libraryList.setArticleTag("springboot");
        libraryList.setArticleOrigin("码猿");
        libraryList.setArticleCode("prXK5YEaGk3rYRZ2");
        libraryListRepository.save(libraryList);
        assertThat(libraryList.getId()).isNotNull();
        LibraryList libraryList1=new LibraryList();
        libraryList1.setLibraryName("R");
        libraryList1.setArticleTitle("数据挖掘与智能决策");
        libraryList1.setLibraryCode("RCode");
        libraryList1.setArticleUrl("Rcode");
        libraryList1.setPublisherAccount("201406114223");
        libraryList1.setPublisher("sunhailin");
        libraryList1.setArticleDate(dateFormat.format(date));
        libraryList1.setLibraryCode("r");
        libraryList1.setArticle("rrrrrrrrrr");
        libraryList1.setArticleTag("r");
        libraryList1.setArticleOrigin("");
        libraryList1.setArticleCode("12345ghj6789");
        libraryListRepository.save(libraryList1);
        assertThat(libraryList1.getId()).isNotNull();
    }
}
