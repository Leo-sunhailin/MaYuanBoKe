package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.ExternalLink;
import cn.my.learning.repository.ExternalLinkRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Administrator on 2017/4/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestExternalLink {
    @Autowired
    private ExternalLinkRepository externalLinkRepository;
    /*private String libraryName;  //类型名
    private String libraryIntro;  //类型简介
    private String libraryCode;  //编号
    private String libraryImg; //类型图片
    private String libraryListUrl; //类型目录链接
    private String addLibraryDate; //类型添加时间*/
    @Test
    public void saveExternalLink(){
        ExternalLink externalLink=new ExternalLink();
        externalLink.setLibraryName("前端");
        externalLink.setLibraryCode("lZAdrF1P");
        externalLink.setLibraryType("前端开发");
        externalLink.setLibraryImg("/image/externalLink/lZAdrF1P.png");
        externalLink.setLibraryListUrl("/externalLink/lZAdrF1P");
        externalLink.setAddLibraryDate(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()));
        externalLinkRepository.save(externalLink);
        assertThat(externalLink.getId()).isNotNull();
        ExternalLink externalLink1=new ExternalLink();
        externalLink1.setLibraryName("GitHub");
        externalLink1.setLibraryCode("OjhSqCUG");
        externalLink1.setLibraryType("软件工程");
        externalLink1.setLibraryImg("/image/externalLink/OjhSqCUG.png");
        externalLink1.setLibraryListUrl("/externalLink/OjhSqCUG");
        externalLink1.setAddLibraryDate(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()));
        externalLinkRepository.save(externalLink1);
        assertThat(externalLink1.getId()).isNotNull();
    }
}
