package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.NoteImage;
import cn.my.learning.repository.ImageRepository;
import cn.my.learning.util.UuidUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 测试用户提交的照片
 * Created by Administrator on 2017/4/9.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestNoteImage {
    @Autowired
    private ImageRepository imageRepository;
    private  String string;
/*    //提交图片的UUID
    private String uploadImgUUID;
    @NotNull
    //提交图片的名字
    private String uploadImgName;
    @NotNull
    //提交图片的绝对路径(服务器)
    private String uploadImgAbsolutePath;
    @NotNull
    //提交图片的格式
    private String uploadImgType;
    @NotNull
    //提交时间
    private String uploadTime;
    @NotNull
    //提交文库用户的用户名
    private String pubName;
    @NotNull
    //是否使用该图片在最终提交或者修改的文章中
    private boolean IsUse;*/

    @Test
    public void testNoteImage(){
        NoteImage noteImage=new NoteImage();
        UuidUtil uuidUtil=new UuidUtil();
        string=uuidUtil.get32UUID();
        noteImage.setUploadImgUUID(string);
        noteImage.setPubName("201406114235");
        noteImage.setUploadImgName("uu.png");
        noteImage.setUploadImgAbsolutePath("E:\\WEB\\MY\\target\\classes\\static\\image\\uploadImage\\201406114235\\uu.png");
        noteImage.setUploadImgType(".png");
        noteImage.setUploadTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        noteImage.setUse(true);
        imageRepository.save(noteImage);
        assertThat(noteImage.getUploadImgUUID()).isNotNull();
    }
}
