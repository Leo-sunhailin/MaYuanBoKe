package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.WebPageHistory;
import cn.my.learning.repository.PageHistoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 测试用户的访问记录（已完成）
 * Created by Administrator on 2017/4/9.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestWebPageHistory {
    @Autowired
    private PageHistoryRepository pageHistoryRepository;
    private String str;
   /* private String id; //ID
    private String SessionId; //SessionID
    private String PageUri; //URI
    private String RemoteAddr; //访问的IP地址
    private String RemoteAddrURL; //访问的IP地址的URL
    private String UserAgent; //UserAgent
    private String VisitTime; //访问时间
    private String Username; //访问的用户*/
    @Test
    public void testWebPageHistory(){
        WebPageHistory webPageHistory=new WebPageHistory();
        webPageHistory.setPageUri("/get_userArticle");
        webPageHistory.setRemoteAddr("127.0.0.1");
        webPageHistory.setRemoteAddrURL("http://localhost:8080/get_userArticle");
        webPageHistory.setUserAgent("空");
        webPageHistory.setVisitTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        webPageHistory.setUsername("ZMagic");
        pageHistoryRepository.save(webPageHistory);
        assertThat(webPageHistory.getId()).isNotNull();
    }

}
