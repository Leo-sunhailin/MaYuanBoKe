package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.User;
import cn.my.learning.repository.UserRepository;
import cn.my.learning.util.PwdEnAndDecode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * 测试保存用户个人信息（已完成）
 * Created by Administrator on 2017/4/7.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestUser {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void testUser(){
        PwdEnAndDecode pwdEnAndDecode = new PwdEnAndDecode();
        /*
        测试是否能保存用户
         */
        User user=new User();
        user.setAccount("201406114235");
        user.setPassword(pwdEnAndDecode.encode("123456"));
        user.setUsername("ZMagic");
        user.setHeadImg("image/userImage/userInfo.png");
        user.setSignature("放飞梦想");
        user.setSex("男");
        user.setEmail("784398237@qq.com");
        user.setPhone("13430141934");
        user.setHomePath("广东省广州市萝岗区九龙镇九龙大道广州商学院");
        user.setRegisteredDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        List<String> list=new ArrayList<>();
        list.add("12345ghj6789");
        user.setArticleCollection(list);
        List<String> list1=new ArrayList<>();
        list1.add("软件工程");
        user.setFollowType(list1);
        userRepository.save(user);
        assertThat(user.getId()).isNotNull();
    }
}
