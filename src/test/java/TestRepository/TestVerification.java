package TestRepository;

import cn.my.learning.MY_Learning;
import cn.my.learning.domain.Verification;
import cn.my.learning.util.EmailUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 测试邮箱验证（已完成）
 * Created by Administrator on 2017/4/10.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MY_Learning.class)
public class TestVerification extends EmailUtil{
    @Autowired
    private MongoTemplate mongoTemplate;
    /*private String verification; //验证码
    private String userAccount; //账户名
    private String userEmailAddress; //用户邮箱地址
    private String start_time; //验证码启用时间
    private String end_time; //验证码失效时间
    private boolean isExpired; //是否失效*/
    EmailUtil emailUtil=new EmailUtil();
    String startTime=emailUtil.getStartTime();
    String endTime=emailUtil.getOutdateTime(startTime);
    public TestVerification() throws ParseException {
    }

    /*@Test
    public void testVerification(){
        Verification verification=new Verification();
        verification.setVerification(emailUtil.coreEmailController("找回密码","还是测试","测试","784398237@qq.com","ZMagic"));
        verification.setUserAccount("201406114235");
        verification.setUserEmailAddress("784398237@qq.com");
        verification.setStart_time(startTime);
        verification.setEnd_time(endTime);
        verification.setIsExpired(false);
        mongoTemplate.save(verification);
        assertThat(verification.getId()).isNotNull();
    }*/
}
